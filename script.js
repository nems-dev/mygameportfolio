startButton = document.querySelector(".play");
diaBox = document.querySelector(".dialog");
gameBox = document.querySelector(".background");
heartBox = document.querySelectorAll(".subheart");
mcImg = document.querySelector(".mc-img");
choiceBox = document.querySelector(".choices");
nextButton = document.querySelector(".next-img")
diaName = document.querySelector(".char-name")
convo = document.querySelector(".convo")
selectGift = document.querySelectorAll('input[name="gift"]')
selectLabel = document.querySelectorAll(".choice-label")
console.log(selectGift[0].checked)

//Initialization
let failstate = false;
let index = 1;
function initialize(){
    failstate=false;
    diaBox.style.display = "none"
    gameBox.style.display = "none"
    mcImg.style.display = "none"
    choiceBox.style.display = "none"
    for (i=0;i<heartBox.length;i++){
        heartBox[i].style.display = "none"
    }
}
initialize();

startButton.addEventListener("click", function(){
    index = 1;
    if (diaBox.style.display == "flex") {
        initialize();
        diaBox.style.display = "none"
        gameBox.style.display = "none"
        mcImg.style.display = "none"
        choiceBox.style.display = "none"
    }
    else{
        diaBox.style.display = "flex";
        gameBox.style.display = "flex";
        diaName.textContent = "Cat";
        diaName.style.color = "yellow";
        convo.textContent = "Meow Meow Meow Meow!";
    }

});
let proceed = true;
console.log(selectGift)

nextButton.addEventListener("click", function(){
    //Traps situation in failstate if answer is wrong
    if(proceed == true){
        index++;
    }
    if(failstate == true){
        diaName.textContent = "Maya";
        diaName.style.color = "Yellow";
        convo.textContent = "I really have to go! Thank you!";
    }
    if (index==1 && failstate == false){
        diaName.textContent = "Me";
        diaName.style.color = "cyan";
        convo.textContent = "Come over here kitty! Let's find your owner...";
    }
    if (index==2 && failstate == false){
        diaName.textContent = "Maya";
        diaName.style.color = "yellow";
        convo.textContent = "Thank you for finding my cat! I was so worried!";
        mcImg.style.display = "flex";
    }
    if (index==3 && failstate == false){
        diaName.textContent = "Me";
        diaName.style.color = "cyan";
        convo.textContent = "Wow you're cute... can I have your number?";
    }
    if (index==4 && failstate == false){
        diaName.textContent = "Maya";
        diaName.style.color = "yellow";
        convo.textContent = "Uhm..... let me think about it... I am going to be late and I need to feed my pet cat.";
    }
    if (index==5 && failstate == false){
        diaName.textContent = "Me";
        diaName.style.color = "cyan";
        convo.textContent = "*Maybe a gift might help...*";
        choiceBox.style.display = "flex"
        selectGift[0].checked = false;
        selectGift[1].checked = false;
        selectGift[2].checked = false;
        selectLabel[0].textContent = "flowers";
        selectLabel[1].textContent = "cat treats";
        selectLabel[2].textContent = "coffee";
        proceed = true;
    }

    if (index==6 && failstate == false){
        if (selectGift[0].checked == false && selectGift[1].checked == false && selectGift[2].checked == false){
            alert("You need to select any of the choices listed below. Click Next to show choices.")
            choiceBox.style.display = "none"
            proceed = false;
            index--;
        }
    }

    if (index==6 && failstate == false){
        choiceBox.style.display = "none"
        for(giftval of selectGift){
            if (giftval.checked){
                gift = giftval.value;
            }
        }
        if (gift == "item2"){
            diaName.textContent = "Maya";
            diaName.style.color = "Yellow";
            convo.textContent = "I love this! I might get a snack before going to the office...";
            heartBox[0].style.display = "flex"
        }
        else{
            failstate = true;
            diaName.textContent = "Maya";
            diaName.style.color = "Yellow";
            convo.textContent = "I really have to go! Thank you!";
            alert("Incorrect! Oh no! Maya left! Please restart the game with the Play button")
        }
    }
    if (index==7 && failstate == false){
        diaName.textContent = "Me";
        diaName.style.color = "cyan";
        convo.textContent = "*Maybe another gift might help...*";
        choiceBox.style.display = "flex";
        selectGift[0].checked = false;
        selectGift[1].checked = false;
        selectGift[2].checked = false;
        selectLabel[0].textContent = "cookies";
        selectLabel[1].textContent = "tea";
        selectLabel[2].textContent = "cash";
        proceed = true;

    }

    if (index==8 && failstate == false){
        if (selectGift[0].checked == false && selectGift[1].checked == false && selectGift[2].checked == false){
            alert("You need to select any of the choices listed below. Click Next to show choices.")
            choiceBox.style.display = "none"
            proceed = false;
            index--;
        }
    }

    if (index==8 && failstate == false){
        choiceBox.style.display = "none"
        for(giftval of selectGift){
            if (giftval.checked){
                gift = giftval.value;
            }
        }
        if (gift == "item1"){
            diaName.textContent = "Maya";
            diaName.style.color = "Yellow";
            convo.textContent = "How did you know? I was starving! This is really appreciated.";
            heartBox[1].style.display = "flex"

        }
        else{
            failstate = true;
            diaName.textContent = "Maya";
            diaName.style.color = "Yellow";
            convo.textContent = "I really have to go! Thank you!";
            alert("Incorrect! Oh no! Maya left! Please restart the game with the Play button")
        }
    }

    if (index==9 && failstate == false){
        diaName.textContent = "Me";
        diaName.style.color = "cyan";
        convo.textContent = "*Hopefully she will like this the most...*";
        choiceBox.style.display = "flex";
        selectGift[0].checked = false;
        selectGift[1].checked = false;
        selectGift[2].checked = false;
        selectLabel[0].textContent = "cash";
        selectLabel[1].textContent = "watch";
        selectLabel[2].textContent = "milk tea";
        proceed = true;
    }

    if (index==10 && failstate == false){
        if (selectGift[0].checked == false && selectGift[1].checked == false && selectGift[2].checked == false){
            alert("You need to select any of the choices listed below. Click Next to show choices.")
            choiceBox.style.display = "none"
            proceed = false;
            index--;
        }
    }

    if (index==10 && failstate == false){
        choiceBox.style.display = "none"
        for(giftval of selectGift){
            if (giftval.checked){
                gift = giftval.value;
            }
        }
        if (gift == "item3"){
            diaName.textContent = "Maya";
            diaName.style.color = "Yellow";
            convo.textContent = "You are very thoughtful! I like you. Here is my number. If you message me I have a surprise for you...";
            heartBox[2].style.display = "flex"

        }
        else{
            failstate = true;
            diaName.textContent = "Maya";
            diaName.style.color = "Yellow";
            convo.textContent = "I really have to go! Thank you!";
            alert("Incorrect! Oh no! Maya left! Please restart the game with the Play button")
        }
    }
    if (index==11 && failstate == false){
        modal.style.display = "block";
    }
});

let modal = document.getElementById("myModal");

let span = document.getElementsByClassName("close")[0];
    
// When the user clicks on <span> (x), close the modal
span.onclick = function() {
  modal.style.display = "none";
}

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
  if (event.target == modal) {
    modal.style.display = "none";
  }
}